//
//  HomeView.swift
//  prova WatchKit Extension
//
//  Created by Luciano Amoroso on 20/01/2020.
//  Copyright :copyright: 2020 Luciano Amoroso. All rights reserved.
//

import SwiftUI
import HealthKit
import UserNotifications

struct HomeView: View {
    @State private var showingSettings = false
    @State private var activeSecurity = false
    private var healthStore = HKHealthStore()
    @State private var value = 0
    let heartRateQuantity = HKUnit(from: "count/min")
    
    @State private var animateStrokeStart = false
    @State private var animateStrokeEnd = true
    @State private var isRotating = false
    @State public var isOn = false
    @State private var bpmOn = false
    @State private var timeRemaining = -1
    @State private var timeaRemaining = -1
    @State private var enableBpm = false
    @State private var media = 100
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
     let timera = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        VStack{
            ZStack{
                Group{
                    if (isOn == true) {
                        Circle()
                            .trim(from: animateStrokeStart ? 1/3 : 1/9, to: animateStrokeEnd ? 2/5 : 1)
                            .stroke(lineWidth: 5)
                            .frame(width: 70, height: 70)
                            .foregroundColor(Color(red: 0.0, green: 1.0, blue: 0.1))
                            .rotationEffect(.degrees(isRotating ? 360 : 0))
                            .onAppear() {
                                withAnimation(Animation.linear(duration: 1).repeatForever(autoreverses: false))
                                   {
                                        self.isRotating.toggle()
                                   }
                                   withAnimation(Animation.linear(duration:
                                   1).delay(0.5).repeatForever(autoreverses: true))
                                   {
                                        self.animateStrokeStart.toggle()
                                   }
                                   withAnimation(Animation.linear(duration:
                                   1).delay(0.5).repeatForever(autoreverses: true))
                                   {
                                        self.animateStrokeEnd.toggle()
                                   }
                    }
                }
                }
                Group {
                    Circle()
                        .foregroundColor(activeSecurity ? Color.init(red: 0, green: 135/256, blue: 0):Color.init(red: 0.2, green: 0.2, blue: 0.2).opacity(0.8))
                        .frame(width: 64, height: 64)
                        
                    Image(activeSecurity ? "shieldON":"shieldOFF")
                }.onTapGesture {
                    if (self.isOn == false ) {
//                        self.timeRemaining = 8
                    self.isOn = true
                    } else {
//                        self.timeRemaining = 8
                    self.isOn = false
                    }
                    
                    if (self.bpmOn == false ) {
                                     self.bpmOn = true
                                     } else {
                                     self.bpmOn = false
                                     }

                self.activeSecurity.toggle()
                if self.activeSecurity{
                    self.autorizeHealthKit()
//                    self.timeRemaining = 8
                    self.timeaRemaining = 55
                    self.startHeartRateQuery(quantityTypeIdentifier: .heartRate)
                                
                    let center = UNUserNotificationCenter.current()
                    center.requestAuthorization(options: [.alert, .sound]) { granted, error in
                        // Enable or disable features based on authorization.
                    }

                }
            }
        }

            Text("Safety")
                .font(.system(size: 25))
                .fontWeight(.bold)
            
            HStack() {
                Text(" ❤️ \(value)")
                    .fontWeight(.bold)
                    .foregroundColor(Color.white) ; Text ("BPM")
//                Text(self.enableBpm ? " :heart: \(value) BPM  " : "")

                  .font(.system(size: 18))
                    .fontWeight(.bold)
                    .foregroundColor(Color.red)
            }
                .foregroundColor(Color.red)
                .padding(.leading, -3.0)
                .onReceive(timer) { _ in
            if self.timeRemaining > 0 {
                self.timeRemaining -= 1
            } else if self.timeRemaining == 0{
                
                self.enableBpm = true

                }
            }
            
            .onReceive(timera) { _ in
                 if self.timeaRemaining > 0 {
                     self.timeaRemaining -= 1
                 } else if self.timeaRemaining == 0{
                     
                     self.printFortnightAvgHeartRate()
                

                     self.timeaRemaining = 35
                    
                        print("la media reale \(self.media) beats per minute")

                     }
                 }
        }.multilineTextAlignment(.center)
    }
    
    func autorizeHealthKit() {
        print("Boh")
        let healthKitTypes: Set = [
        HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!]

        healthStore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { _, _ in }
        
    }
    
    private func startHeartRateQuery(quantityTypeIdentifier: HKQuantityTypeIdentifier) {
        
        // 1
        let devicePredicate = HKQuery.predicateForObjects(from: [HKDevice.local()])
        print("Ciao")
        // 2
        let updateHandler: (HKAnchoredObjectQuery, [HKSample]?, [HKDeletedObject]?, HKQueryAnchor?, Error?) -> Void = {
            query, samples, deletedObjects, queryAnchor, error in
            
            // 3
            guard let samples = samples as? [HKQuantitySample] else {
                return
            }
            
             if (self.bpmOn == true) {
            self.process(samples, type: quantityTypeIdentifier)
            }
        }
        
        // 4
        let query = HKAnchoredObjectQuery(type: HKObjectType.quantityType(forIdentifier: quantityTypeIdentifier)!, predicate: devicePredicate, anchor: nil, limit: HKObjectQueryNoLimit, resultsHandler: updateHandler)
        
        query.updateHandler = updateHandler
        
        // 5
        
        if (self.bpmOn == true) {
        healthStore.execute(query)
        }
    }
    
   private func printFortnightAvgHeartRate() {
        let calendar = Calendar.current

        let heartRateType = HKQuantityType.quantityType(forIdentifier: .heartRate)!

        let endDate = Date()
        let startDate = calendar.date(byAdding: .minute, value: -5, to: endDate)!

        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: [])

        // Set the anchor to exactly midnight
        let anchorDate = Date()

        var interval = DateComponents()
        interval.minute = 1

        // Create the query
        let query = HKStatisticsCollectionQuery(quantityType: heartRateType,
                                                quantitySamplePredicate: predicate,
                                                options: .discreteAverage,
                                                anchorDate: anchorDate,
                                                intervalComponents: interval)

        // Set the results handler
        query.initialResultsHandler = { query, results, error in
            guard let statsCollection = results else { return }

            for statistics in statsCollection.statistics() {
                guard let quantity = statistics.averageQuantity() else { continue }

                let beatsPerMinuteUnit = HKUnit.count().unitDivided(by: HKUnit.minute())
                let value = quantity.doubleValue(for: beatsPerMinuteUnit)

                print("the average heart rate was \(value) beats per minute")
                
                self.media = Int(value)
                
            }
        }

        HKHealthStore().execute(query)
    }
    
    private func process(_ samples: [HKQuantitySample], type: HKQuantityTypeIdentifier) {
        var lastHeartRate = 0.0
        
        for sample in samples {
            if type == .heartRate {
                lastHeartRate = sample.quantity.doubleValue(for: heartRateQuantity)
                
                var bpmh = lastHeartRate*0.10
                
                if ((Int(lastHeartRate) - Int(bpmh)) < self.media) {
                    
                    //mandare notifica
                    self.notifica()
                }
                print(":heart:Last heart rate was: \(lastHeartRate)")
            }
            
            self.value = Int(lastHeartRate)
        }
    }
    
    private func notifica () {
        let content = UNMutableNotificationContent()
                           content.title = "Driver Aid"
                           content.subtitle = "Hey, wake up!"
                           content.body = "Don't go asleep"
                           content.badge = 1
                           let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
                           let request = UNNotificationRequest(identifier: "timerDone", content: content, trigger: trigger)
                           UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
