//
//  SetSoundView.swift
//  prova WatchKit Extension
//
//  Created by Federico Rotoli on 16/01/2020.
//  Copyright :copyright: 2020 Federico Rotoli. All rights reserved.
//

import SwiftUI
import AVFoundation

struct SetSoundView: View {
    @State private var song: String!
    @State private var didTap = [false, false, false, false]
    @State private var sound = AVAudioPlayer()
    
    var body: some View {
        ScrollView {
            VStack{
                Text("Select the sound of the aid")
                    .font(.system(size: 12))
                    .foregroundColor(Color.gray)
                    .offset(y:6)
                
                Button(action: {
                    self.name()
                    self.playSound(name: "pitchSound")
                }) {
                    Text("High Pitch Sound")
                }
                .accentColor(didTap[0] ? Color.init(red: 0.0, green: 1.0, blue: 0.0) : Color.white)
                Button(action: {
                    
                    UserDefaults.standard.set("Luciano", forKey: "Song") as? String
                    self.name()
                }) {
                    Text("Luciano")
                }
                .accentColor(didTap[1] ? Color.init(red: 0.0, green: 1.0, blue: 0.0) : Color.white)
                Button(action: {
                    UserDefaults.standard.set("Giovanni", forKey: "Song") as? String
                    self.name()

                }) {
                      Text("Giovanni")
                }
                .accentColor(didTap[2] ? Color.init(red: 0.0, green: 1.0, blue: 0.0) : Color.white)
                Button(action: {
                    UserDefaults.standard.set("Davide", forKey: "Song") as? String
                    self.name()
                }) {
                    Text("Davide")
                }
                .accentColor(didTap[3] ? Color.init(red: 0.0, green: 1.0, blue: 0.0) : Color.white)
                if song != nil {
                    Text("Your choosen sound is: \(song)")
                        .font(.system(size: 8))
                }
            }
                
        }
        .onAppear(perform: name)
        .navigationBarTitle("Close")
    }
    
    func playSound(name: String) {
        let path = Bundle.main.path(forResource: "\(name).mp3", ofType: nil)
        do {
            sound = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path!))
            sound.play()
        } catch {
            print( "Could not find file")
        }
    }
    
    func name(){
        guard let selected = UserDefaults.standard.string(forKey: "Song") else {return}
        
        self.song = selected
        switch song{
        case "pitchSound":
            didTap = [true,false,false,false];
        case "Luciano":
            didTap = [false,true,false,false];
        case "Giovanni":
            didTap = [false,false,true,false];
        case "Davide":
            didTap = [false,false,false,true];
        default:
            didTap = [false,false,false,false]
        }
    }
}

struct SetSoundView_Previews: PreviewProvider {
    static var previews: some View {
        SetSoundView()
    }
}
