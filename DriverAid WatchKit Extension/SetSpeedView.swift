import SwiftUI
import CoreLocation
import Combine
import UserNotifications

class LocationManager: NSObject, CLLocationManagerDelegate, ObservableObject {
    
    var location2 = CLLocation()
    private let geocoder = CLGeocoder()
    private let locationManager = CLLocationManager()
    let objectWillChange = PassthroughSubject<Void, Never>()
    
    @Published var status: CLAuthorizationStatus? {
      willSet { objectWillChange.send() }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.status = status
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        self.location = location
        
        if location2 != nil {
            if (abs(location2.latitude - location.latitude) + abs(location2.longitude - location.longitude) > 0.000001) {
                print("\(abs(location2.latitude - location.latitude) + abs(location2.longitude - location.longitude))")
                
                
                let center = UNUserNotificationCenter.current()
                center.requestAuthorization(options: [.alert, .sound]) { granted, error in
                    // Enable or disable features based on authorization.
                }
                
                let content = UNMutableNotificationContent()
                content.sound = .defaultCritical
                content.title = "Driver Aid"
                content.subtitle = "Speed Checker"
                content.body = "You are going too fast!"
                content.badge = 1
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                let request = UNNotificationRequest(identifier: "timerDone", content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }
        }
        
        location2 = location
        
        
        
    }

    @Published var location: CLLocation? {
      willSet { objectWillChange.send() }
    }

    override init() {
      super.init()

      self.locationManager.delegate = self
      self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
      self.locationManager.requestWhenInUseAuthorization()
      self.locationManager.startUpdatingLocation()
    }
    
    @Published var placemark: CLPlacemark? {
      willSet { objectWillChange.send() }
    }
    
    private func geocode() {
      guard let location = self.location else { return }
      geocoder.reverseGeocodeLocation(location, completionHandler: { (places, error) in
        if error == nil {
          self.placemark = places?[0]
        } else {
          self.placemark = nil
        }
      })
    }
}

struct SetSpeedView: View {
    
    
    
    
    @Environment(\.presentationMode) var presentationMode
    @State var sliderValue = 0.0
    var minimumValue = 0.0
    var maximumvalue = 100.0
    @State var locationManager: CLLocationManager = CLLocationManager()
    @State var timer2 = Timer()

    @ObservedObject var lm = LocationManager()
    
    var body: some View {
        VStack {
            Text ("Notification Setter")
                .font(.headline)
                .foregroundColor(Color.gray)
                .padding([.top, .leading])
            HStack {
//                       Text("\(Int(minimumValue))")
                       // 2.
                Slider(value: $sliderValue, in: minimumValue...maximumvalue, step: 10)
                    .padding(.vertical)
                    .accentColor(.green)
//                          Text("\(Int(maximumvalue))")
            }
            
                   // 3.
            Text("\(Int(sliderValue)) km/h")
            .font(.system(size: 18))
            .fontWeight(.bold)
            .foregroundColor(Color.green)
        
            Button(action: {
//                self.startDeviceMotion()
                UserDefaults.standard.set(self.sliderValue, forKey: "Speed") as? Double
                self.presentationMode.wrappedValue.dismiss()
            
            }) {
                Text("Done")
                    .font(.headline)
                    .foregroundColor(Color.green)
            }
            .padding(.vertical, 0.0)
        
        }
        .accentColor(.green)
        .onAppear(perform: value)
            
        .navigationBarTitle("Close")
    }
    
    
    func value() {
        guard let speedValue = UserDefaults.standard.value(forKey: "Speed") else { return }
        sliderValue = speedValue as! Double
    }
}

extension CLLocation {
    var latitude: Double {
        return self.coordinate.latitude
    }
    
    var longitude: Double {
        return self.coordinate.longitude
    }
}




struct SetSpeedView_Previews: PreviewProvider {
    static var previews: some View {
        SetSpeedView()
    }
}
