//
//  SetWakerView.swift
//  prova WatchKit Extension
//
//  Created by Federico Rotoli on 16/01/2020.
//  Copyright © 2020 Federico Rotoli. All rights reserved.
//

import SwiftUI
import UserNotifications

struct SetWakerView: View {
    @Environment(\.presentationMode) var presentationMode
    @State private var activeWaker = false
    @State private var selectedTime = 0
    var times = ["15 min","30 min","45 min","60 min"]

    var body: some View {
        VStack{
            Text("Waker Notification")
                .font(.headline)
                .foregroundColor(Color.gray)
            ZStack{
                Rectangle()
                    .frame(width: 160, height: 40)
                    .foregroundColor(Color.init(red: 0.2, green: 0.2, blue: 0.2))
                    .cornerRadius(10)
                Toggle(isOn: $activeWaker) {
                    Text("Waker")
                        .font(.headline)
                        
                }.padding()
                
            }
            
            Spacer()
            
            if activeWaker {
                HStack{
                    Text("Timer")
                        .font(.headline)
                        .padding()
                        .offset(y:8)
                    Picker(selection: $selectedTime, label: Text("")) {
                        ForEach(0 ..< times.count) {
                            Text(self.times[$0])

                        }
                    }.padding(.trailing)
                }.offset(y: -6)
            }
            
            Button(action: {
                if self.activeWaker{
                    UserDefaults.standard.set(String(self.selectedTime), forKey: "Waker") as? String
                    UserDefaults.standard.set(self.times[self.selectedTime], forKey: "WakerTimer") as? String
                        
                        self.notifica()
                } else {
                    UserDefaults.standard.set(nil, forKey: "Waker") as? String
                    UserDefaults.standard.set(nil, forKey: "WakerTimer") as? String
                }
                self.presentationMode.wrappedValue.dismiss()
            }
            ) {
                Text("Done")
                    .font(.headline)
            }
            .cornerRadius(10)


        }
        .padding(.top, 5.0)
            .accentColor(.green)
        .onAppear(perform: controllo)
    }
    
    func notifica() {
        
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { granted, error in
            // Enable or disable features based on authorization.
        }
        
        let numero = self.times[self.selectedTime].components(separatedBy: " ")
        
        let content = UNMutableNotificationContent()
        content.sound = .defaultCritical
        content.title = "Driver Aid"
        content.subtitle = "Waker Checker"
        content.body = "A little reminder for your safety"
        content.badge = 1
        
        let timer = Double(numero[0])!*60
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timer, repeats: activeWaker)
        let request = UNNotificationRequest(identifier: "timerDone", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    
    func controllo() {
        guard let minuti = UserDefaults.standard.string(forKey: "Waker") else {return}
        activeWaker = true
        selectedTime = Int(minuti)!
    }
}

struct SetWakerView_Previews: PreviewProvider {
    static var previews: some View {
        SetWakerView()
    }
}
