//
//  SettingsView.swift
//  prova WatchKit Extension
//
//  Created by Luciano Amoroso on 20/01/2020.
//  Copyright :copyright: 2020 Luciano Amoroso. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    @State private var setSpeed = false
    @State private var setSound = false
    @State private var setWaker = false

    var body: some View {
           VStack{
            ZStack{
                Group{
                    Rectangle()
                        .padding(0.0)
                        .frame(width: 150, height: 50)
                        .cornerRadius(8)
                        .foregroundColor(setSpeed ? Color.init(red: 0, green: 135/256, blue: 0):Color.init(red: 0.2, green: 0.2, blue: 0.2).opacity(0.8))
                    ZStack {
                        Circle()
                              .padding(.leading, -104.0)
                          .frame(width: 32, height: 32)
                              .foregroundColor(Color.red)
                        Image(systemName: "timer")
                            .padding(.leading, -60.0)
                    }
                    Text("Set Speed")
                        .font(.system(size: 19))
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                        .multilineTextAlignment(.trailing)
                        .padding(.trailing, -31.0)
                }
             
            }
            .padding(.top, 0.0)
                .onTapGesture {
                    self.setSpeed.toggle()
                }
                .sheet(isPresented: $setSpeed) {
                    SetSpeedView()
                }
            ZStack{
                Group{

                    Rectangle()
                        .padding(0.0)
                        .frame(width: 150, height: 50)
                        .cornerRadius(8)
                        .foregroundColor(setWaker ? Color.init(red: 0, green: 135/256, blue: 0):Color.init(red: 0.2, green: 0.2, blue: 0.2).opacity(0.8))
                    ZStack {
                        Circle()
                              .padding(.leading, -104.0)
                          .frame(width: 32, height: 32)
                            .foregroundColor(Color.purple)
                        Image(systemName: "hourglass")
                            .padding(.leading, -57)
                    }
                    Text("Set Waker")
                        .font(.system(size: 19))
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                        .multilineTextAlignment(.trailing)
                        .padding(.trailing, -31.0)
                }
             
            }
                .padding(.top, 15.0)
                .onTapGesture {
                    self.setWaker.toggle()
                }
                .sheet(isPresented: $setWaker) {
                    SetWakerView()
                }
                

        }
        
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
